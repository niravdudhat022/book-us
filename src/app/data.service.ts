import { HttpClient } from '@angular/common/http'; 
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class DataService {

   constructor(private http: HttpClient) {
        
    }

    public getCityList(): Observable<any> {
        return this.http.get("./assets/data/location.json");
    }

    public getSalesList(): Observable<any> {
        return this.http.get("./assets/data/sales.json");
    }

    public getMarketingSource(): Observable<any> {
        return this.http.get("./assets/data/MarketingSource.json");
    }

    public getCurrentCash(): Observable<any> {
        return this.http.get("./assets/data/CashInDrawer.json");
    }
    
}
