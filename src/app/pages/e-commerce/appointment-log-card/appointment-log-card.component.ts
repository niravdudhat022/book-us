import { Component, Input } from '@angular/core';
import { ProgressInfo, StatsProgressBarData } from '../../../@core/data/stats-progress-bar';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'ngx-appointment-log-card',
  styleUrls: ['./appointment-log-card.component.scss'],
  templateUrl: './appointment-log-card.component.html',
})
export class EAppointmentLogCardCardComponent {

  cancelledData :any=[];
  noShowData :any=[];

  allData:any=[];
  @Input('data') 
  set setData(data:any){
    if(data){
      this.allData=data;
      this.cancelledData=this.allData;
      this.noShowData=this.allData;
    }
  }

  private alive = true;

  progressInfoData: ProgressInfo[];

  constructor(private statsProgressBarService: StatsProgressBarData) {
    this.statsProgressBarService.getProgressInfoData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.progressInfoData = data;
      });
  }

  ngOnDestroy() {
    this.alive = true;
  }
}
