import { Component } from '@angular/core';
import { DataService } from 'app/data.service';

@Component({
  selector: 'ngx-ecommerce',
  templateUrl: './e-commerce.component.html',
})
export class ECommerceComponent {

  customerGenderData:any=[
    {
      "label":"Male",
      "percentage":"75"
    },
    {
      "label":"Female",
      "percentage":"25"
    }
  ];
  newOldData:any=[
    {
      "label":"New",
      "percentage":"65"
    },
    {
      "label":"Old",
      "percentage":"35"
    }
  ];
  topSpenderCustomer:any=[
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    },
    {
      "name":"Test",
      "spent":"60000"
    }
  ];
  awaitedCustomerList:any=[
    {
      "name":"Test",
      "time":"16",
      "phone":"9933445566"
    },
    {
      "name":"Test",
      "time":"16",
      "phone":"9933445566"
    },
    {
      "name":"Test",
      "time":"16",
      "phone":"9933445566"
    },
    {
      "name":"Test",
      "time":"16",
      "phone":"9933445566"
    },
    {
      "name":"Test",
      "time":"16",
      "phone":"9933445566"
    }
  ];
  appointmentData:any=[
    {
      "name":"Test",
      "date":new Date(),
      "status":"noShow"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"noShow"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"noShow"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"Cancelled"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"Cancelled"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"Cancelled"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"Late"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"Late"
    },
    {
      "name":"Test",
      "date":new Date(),
      "status":"Late"
    }
  ];
  selectedCity:any;
  salesData:any=[];

  constructor(public dataService : DataService){
  }

  onCityChange(event){
    if(event.data){
      this.salesData=event.data;
    }
    if(event.city && event.allCity){
      this.selectedCity=event;
    }
  }

  prepareMarketingSourceData(){

  }

}
