import { Component, Input } from '@angular/core';
import { ProgressInfo, StatsProgressBarData } from '../../../@core/data/stats-progress-bar';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'ngx-average-awaited-time-card',
  styleUrls: ['./average-awaited-time-card.component.scss'],
  templateUrl: './average-awaited-time-card.component.html',
})
export class EAverageAwaitedTimeCardComponent {

  @Input('data') data :any;

  private alive = true;

  progressInfoData: ProgressInfo[];

  constructor(private statsProgressBarService: StatsProgressBarData) {
    this.statsProgressBarService.getProgressInfoData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.progressInfoData = data;
      });
  }

  ngOnDestroy() {
    this.alive = true;
  }
}
