import { Component, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { DataService } from 'app/data.service';
import * as _ from 'lodash';

@Component({
  selector: 'ngx-current-cash',
  styleUrls: ['./current-cash.component.scss'],
  templateUrl: './current-cash.component.html',
})
export class ECurrentCashComponent {
  data: any;
  options: any;
  themeSubscription: any;
  cashData:any={};
  @Input('selectedCity')
  set setCity(cityData:any){
    if(cityData && cityData.city && cityData.allCity){
      const index = _.findIndex(cityData.allCity, (o) => { return o.id == cityData.city; })
      this.cashData['cityName'] = cityData.allCity[index]['locationName'];
      this.getCurrentCash(cityData.city);
    }
  }

  constructor(private theme: NbThemeService,public dataService : DataService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: ['Download Sales', 'In-Store Sales', 'Mail Sales'],
        datasets: [{
          data: [300, 500, 100],
          backgroundColor: [colors.primaryLight, colors.infoLight, colors.successLight],
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  getCurrentCash(selectedCity){
    this.dataService.getCurrentCash().subscribe((data)=>{
      if(data && data.data){
        const index = _.findIndex(data.data, (o) => { return o.locationId == selectedCity; });
        this.cashData['cash']=data.data[index]['cash']
      }
    })
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
