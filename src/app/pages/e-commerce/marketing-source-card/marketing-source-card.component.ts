import { Component, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { DataService } from 'app/data.service';
import * as  _ from 'lodash';

@Component({
  selector: 'ngx-marketing-source-card',
  styleUrls: ['./marketing-source-card.component.scss'],
  templateUrl: './marketing-source-card.component.html',
})
export class EMarketingSourceCardComponent {
  options: any;
  themeSubscription: any;
  filteredSalesData: any = [];
  @Input('salesData')
  set salesData(data: any) {
    if (data && data.length != 0) {
      this.filteredSalesData = data;
      this.getMarketingSource();
    }
  }
  data: any;
  sectionType: any;
  marketingSource: any = [];


  constructor(private theme: NbThemeService, public dataService: DataService) {

  }

  setChartData(label: any,data:any,color:any) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;

      this.data = {
        labels: label,
        datasets: [{
          data: data,
          backgroundColor: color,
        }],
      };

      this.options = {
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              display: false,
            },
          ],
          yAxes: [
            {
              display: false,
            },
          ],
        },
        legend: {
          labels: {
            fontColor: chartjs.textColor,
          },
        },
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
  getMarketingSource() {
    this.dataService.getMarketingSource().subscribe((data: any) => {
      if (data && data.data) {
        this.marketingSource = data.data;
        const label = [];
        const chartData = [];
        const color = [];
        for (let index = 0; index < this.marketingSource.length; index++) {
          label.push(this.marketingSource[index]['Type']);
          let count = 0;
          for (let j = 0; j < this.filteredSalesData.length; j++) {
            if(this.filteredSalesData[j]['source']==this.marketingSource[index]['id']){
              count=count+1;
            }
          }
          chartData.push(count);
          const colorCode = '#'+index+'E3A'+index+'C';
          color.push(colorCode)
        }
        this.setChartData(label,chartData,color);
      }
    })
  }
}
