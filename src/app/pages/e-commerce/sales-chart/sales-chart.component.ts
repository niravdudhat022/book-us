import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { DataService } from 'app/data.service';
import * as  _ from 'lodash';
import * as  moment from 'moment';

@Component({
  selector: 'ngx-sales-chart',
  styleUrls: ['./sales-chart.component.scss'],
  templateUrl: './sales-chart.component.html',
})
export class ESalesChartComponent {
  public type: string = 'Week';
  public lineChartData: ChartDataSets[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{
        type: 'time',
        time: {
          unit: 'day'
        }
      }],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        },
      ]
    },
    annotation: {
      annotations: [
      ],
    },
  };
  public lineChartColors: Color[] = [
    {
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];
  public types: string[] = ['Week', 'Month', 'Year'];
  public weekLabel: string[] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  public monthLabel: string[] = [
    'Jan', 'Feb', 'Mar',
    'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec',
  ];
  public yearLabel: string[] = [
    '2012',
    '2013', '2014', '2015',
    '2016', '2017', '2018',
    '2019', '2020'
  ]
  public lineChartLabels: Label[] = this.monthLabel;
  public cityList: any = [];
  public salesList: any = [];
  public city: number = 1;
  public month: string = 'Apr';
  public displayMonthDropDown : boolean = false;
  @Output() cityChange = new EventEmitter<any>();

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(public dataService: DataService) { }

  ngOnInit(): void {

    this.getCityList();
  }

  public getCityList() {
    this.dataService.getCityList().subscribe((data: any) => {
      if (data && data.data) {
        this.cityList = data.data;
        this.getSalesList();
      }
    })
  }

  public getSalesList() {
    this.dataService.getSalesList().subscribe((data: any) => {
      if (data && data.data) {
        this.salesList = data.data;
        this.filterSalesData();
      }
    })
  }

  filterSalesData() {
    const salesData = [];
    const dataByCity = [];
    for (let index = 0; index < this.salesList.length; index++) {
      if (this.salesList[index]['cityId'] == this.city) {
        dataByCity.push(this.salesList[index]);
        salesData.push({
          x: moment(this.salesList[index]['date']).format('YYYY-MM-DD'),
          y: this.salesList[index]['sales']
        })
      }
    }
    this.lineChartData = [
      {
        data: salesData, label:this.cityList[_.findIndex(this.cityList, (o) => { return o.id == this.city; })]['locationName']
      },
    ];
    this.cityChange.emit({city:this.city,allCity:this.cityList,data:dataByCity});
    this.chart.update();
  }

  private generateNumber(i: number): number {
    return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  }

  public hideOne(): void {
    const isHidden = this.chart.isDatasetHidden(1);
    this.chart.hideDataset(1, !isHidden);
  }

  public pushOne(): void {
    this.lineChartData.forEach((x, i) => {
      const num = this.generateNumber(i);
      const data: number[] = x.data as number[];
      data.push(num);
    });
    this.lineChartLabels.push(`Label ${this.lineChartLabels.length}`);
  }

  public changeColor(): void {
    this.lineChartColors[2].borderColor = 'green';
    this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
  }

  public changeLabel(): void {
    this.lineChartLabels[2] = ['1st Line', '2nd Line'];
  }

  changePeriod(period: string): void {
    this.type = period;
    if (period == 'Month') {
      this.displayMonthDropDown =  true;
    }
    else {
      this.displayMonthDropDown = false;
    }
    this.chart.update();
  }

  changeCity(city: any): void {
    this.city = city;
    this.filterSalesData();
  }
}
